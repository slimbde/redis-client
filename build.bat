@echo off

docker stop -t0 redis-client
docker build -t slimbde/redis-client .
docker run --rm -d -p 4200:4200 --net test-network --name redis-client slimbde/redis-client
