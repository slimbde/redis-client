const express = require("express")
const app = express()
const cors = require("cors")
const axios = require("axios")
const Redis = require("redis")

const redisClient = Redis.createClient({
  socket: {
    port: 6379,
    host: "redis",
  },
  password: "redis",
})
redisClient.connect()
redisClient.on('error', err => console.log('Redis: error: ', err.message))
redisClient.on('connect', () => console.log('Redis-client: Connected to redis server'))

const PORT = process.env.PORT || 4200
const DEFAULT_EXPIRATION = 3600

app.use(cors({ origin: `http://localhost:${PORT}` }))
app.use(express.json()) //// to be able to reach req.body



app.get("/api/posts", async (req, res) => {
  const start = process.hrtime()
  const { data } = await axios.get(`https://jsonplaceholder.typicode.com/posts`)
  const elapsed = process.hrtime(start)[1] / 1_000_000
  console.log(elapsed.toFixed(3) + " ms")

  redisClient.setEx("posts", DEFAULT_EXPIRATION, JSON.stringify(data))

  res.send(data)
})

app.get("/", (req, res) => res.send("healthy"))

app.listen(PORT, () => console.log(`Server has been started on http://localhost:${PORT} ...`))